""" @file               BNO055.py
    @brief              A file for the BNO055 IMU from Bosch. 
    @details            Contains the class BNO055 which provides IMU calibration and testing.
    @author             Kendall Chappell
    @author             Tim Matista
    @date               11/2/2021
"""


#import pyb
import ustruct
import os

class BNO055:
    """ @brief      Class for the Bosch BNO055 IMU             
        @details    Allows for the IMU to have the operating mode set or read, 
                    read the calibration status, recognize when the calibration is complete
                    and read Euler angles (heading, pitch, and roll) in degrees. 
                    Allows for the angular velocities to be read. 
    """
    def __init__(self, master_object, devAd):
        """ @brief       Inititalizes IMU object       
            @details     Takes in the master object to communicate with the IMU.
            @param       master_object      The I2C controller object
            @param       devAd              The device address
        """           
        self.i2c = master_object                 
        self.eulerAd = 0x1A                     #the first address corresponding to the Euler angles
        self.modeAd = 0x3D                      #the address of the operation mode, write to this to change operation mode
        self.accAd = 0x28                       #the first address corresopnding to the accleration data        <--- check this
        self.calStatAd = 0x35                   #the address corresponding to the calibration status
        self.calCoeffAd = 0x55                  #the address corresponding to the calibration coefficients
        self.gyrAd = 0x14                       #the first address corresponding to the gyroscope data <---- check this
        self.devAd = devAd                      #the address of the BNO055                                       <----- check this
        
        print("i2c created")    #<---- only for testing
        
    def prepIMU(self):
        while True:
            if self.calStat() != 0b11111111:

                if 'calCoeffSaved' in os.listdir():
                    self.opMode(1)
                    with open('calCoeffSaved', 'r') as f:                               
                        self.setCalCoeff(f.read())                                     
                    self.opMode(12) 
                    print(self.calStat())
                else:
                    print("no file name 'calCoeffSaved' see in folder. Making new calibration file")
                    newFile = open("calCoeffSaved", 'w')
                    newFile.write(self.getCalCoeff())
                    newFile.Close()
                    
                #make file here
            else:
                print("Calibration complete")
                break
        
        #check if IMU is calibrated
            #check if there is a file with a specific name in the same folder
            #   read data from file
            #   write data to calibration coefficnets
            #   check if device is calibrated
            #   annouce success
            #Annouce calibration is needed
            #   collect calibration data
            #       check if calibration is complete
            #       Prepare a file for calibration data
            #       write data to file
            #       Annouce success



    
    
    def opMode(self, mode):
        """ @brief          Sets the IMU operating mode            
            @details        Sets the OPR_MODE register on the BNO055 
            @param  mode   Opperating mode of the IMU
        """         
        self.i2c.mem_write(mode, self.devAd, self.modeAd)
    
        
    def calStat(self):
        """ @brief      Get the IMU calibration status         
            @details    Reads the value of the CALIB_STAT register.
            @return     returns the calibration status        
        """                
        
        return self.i2c.mem_read(1, self.devAd, self.calStatAd)
        
    def getCalCoeff(self):
        """ @brief      Gets the current calibration coefficients of the IMU           
            @details    Gets the values stored in the calibration coefficents registers.
            @return     22 bytes corresponding to the calibration coefficients
        """    
 
        return self.i2c.mem_read(22, self.devAd, self.calCoeffAd)
    
    
    def setCalCoeff(self, calCoeff):
        """ @brief      Set the calibration coefficients of the IMU           
            @details    Write input calCoeff data into the appropriate registers
            @param      calCoeff    the 22 bytes that represent the calibration coefficients
        """
        self.i2c.mem_write(calCoeff, self.devAd, self.calCoeffAd)
    
    def Euler(self):
        """ @brief      Reads the Euler angles (degrees)           
            @details    Returns a list of the Euler angles in the order (heading, roll, pitch)
        """    
          
        eul_bytes = self.i2c.mem_read(6, self.devAd, self.eulerAd) #/16)                #<---- check this devision, may not work at all
        (heading, pitch, roll) = ustruct.unpack('hhh', eul_bytes) #/16)                #<---- check this devision, may not work at all
        
        return heading/16, pitch/16, roll/16
        
        #should this even be here?
#        if heading > 32767:
#            heading -= 65536
#        if roll > 32767:
#            roll -= 65536
#        if pitch > 32767:
#            pitch -= 65536

        
    def angVel(self):
        """ @brief         Reads the angular velocity        
            @details      Returns the angle velocity  in the order (x, y, z)
        """    


        
        vels = self.i2cController.mem_read(6, self.devAd, self.gyrAd)
        (x, y, z) = ustruct.unpack('hhh', vels)
        return x/16, y/16, z/16
        
        
if __name__ == 'main':
    print("test")
