""" @file           Task_Motor.py
    @brief          Motor task for utilizing Motor class
    @details        Create motor object alongide driver. Periodically updates duty cycle for PWM signal
    @author         Tim Matista
    @author         Kendall Chapell
    @date           10/12/2021

"""


#import statements
import utime


class Task_Motor():
    ''' @brief      Motor task for controlling Motor class
        @details    Create motor object alongside driver class. Periodically updates duty cycle.
    '''
    def __init__(self, motorID,  DC_val, faultFlagShare, period, motorDriver):
        ''' @brief              Define target motor and driver information            
            @param              period     Period that the task runs at, in microseconds.
            @param              motorID    Designates target Motor object 
            @param              DC_val     Value of duty change for motor
            @param              faultFlagShare share object denoting a fault has been detected
            @param              motorDriver   Designated driver that will control motor
         '''
        
        self.motorID = motorID
        self.DC_val = DC_val
        self.faultFlagShare = faultFlagShare 
        self.motorDriver = motorDriver
        self.currentMotor = self.motorDriver.motor(motorID)
        self.period = period
        self.nextTime = utime.ticks_add(utime.ticks_us(), self.period)
        
       
        
    def run(self):
        ''' @brief      Runs the motor task.
        '''

         #check if time when run has passed the "next time" the task should run
        if utime.ticks_diff(utime.ticks_us(), self.nextTime) >= 0:
            self.nextTime = utime.ticks_add(self.nextTime, self.period)
            self.currentMotor.set_duty(self.DC_val.read())  
            if not self.faultFlagShare.read():
                self.motorDriver.enable()
