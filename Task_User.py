""" @file           Task_User.py
    @brief          Encoder task for user input
    @details        Periodically check for user input related to encoder task <------- and motor task?
    @author         Tim Matista
    @author         Kendall Chapell
    @date           10/15/2021
"""

#consider passing in data tuples rather than constructing here. would cut down on share objects, paramaters, etc

#Constants for positions of data in tuples
tim = 0
pos = 1
delta = 2
omega = 1
actuation = 2


#import statements
import utime
import pyb
import math
from micropython import const


#designate states
    #add new state?
S0_init = const(0)
S1_wait = const(1)
S2_collect = const(2)
S3_print = const(3) 
S4_duty = const(4)
S5_collectSR = const(5)
S6_printSR = const(6)

#placehlder for when no command needs to be run
NO_COMMAND = 0

#Ticks per revolution of encoders
TPR = 4000

class Task_User:
    """ @brief         Creates a user interface 
        @details       Allows the user to input commands through the keyboard
                       which will be interpretted by this file. 
    """
    
    def __init__(self, period_check, period_data, 
                     z_flag_1, z_flag_2,
                     data_1, data_2,
                     faultFlagShare,
                     DC_val_1, DC_val_2,
                     o_meas_1, o_meas_2,
                     o_ref_1, o_ref_2,
                     closedLoop1, closedLoop2,
                     dataQueue, srQueue):
        
               
        """ @brief      Initializes a user task.
            @param      period_check   Period, in microseconds, at which to check for new user commands
            @param      period_data    Period, in microseconds, at which to record encoder data
            @param      pos_1          Encoder 1 position
            @param      del_1          Encoder 1 delta (change in position)
            @param      z_flag_1       Encoder 1 zero flag, calls for encoder 1 to be zeroed
            @param      pos_2          Encoder 2 position
            @param      del_2          Encoder 2 delta (change in position)
            @param      z_flag_2       Encoder 2 zero flag, calls for encoder 2 to be zeroed
            @param      faultFlagShare Flag denoting fault detection
        """
#outdated documentation of parameters
        
        self.ser = pyb.USB_VCP()                                                                #serial input from user


        self.period_check = period_check                                                        #period for program to check for input
        self.period_data = period_data                                                          #period for program to collect data
        
        self.z_flag_1 = z_flag_1
        self.z_flag_2 = z_flag_2

        self.faultFlagShare = faultFlagShare
        
        self.next_check = utime.ticks_add(utime.ticks_us(), self.period_check)
        self.next_data = utime.ticks_add(utime.ticks_us(), self.period_data)

        
        self.DC_val_1 = DC_val_1
        self.DC_val_2 = DC_val_2
        
        self.data_1 = data_1
        self.data_2 = data_2
        
        self.o_meas_1 = o_meas_1
        self.o_meas_2 = o_meas_2
        
        self.o_ref_1 = o_ref_1
        self.o_ref_2 = o_ref_2
        
        self.closedLoop1 = closedLoop1
        self.closedLoop2 = closedLoop2
        
        self.dataQueue = dataQueue
        self.srQueue = srQueue
        
        self.command = NO_COMMAND
        self.state = S0_init
        
        self.runs = 0                                                                           #currently unused, tracks how many times the program has run <----------
        
        self.dataLen = 30e6 / period_data + 1 
        self.srLen = 10e6 / self.period_data +1
        
        self.my_str = ""       #empty number string for user input
         
        self.duty = None         #duty cycle value
        self.Kp = None           #proportional gain
        self.omega = None
        
        self.dataBin = []
                                                                      
#        self.plotTime = []                <-------for plotting
#        self.plotVel = []
#        self.dataFile = open("dataFile.txt", 'w')
        
        self.dataFile = open("dataFile.txt", "a")
        
    def run(self):
        """ @brief Runs one iteration of the FSM
        """
        
    
        if self.state == S0_init:                                                               #bootup state, prints instructions for user interface
            print("Program initiated \n"
                 "z: Set encoder 1 position to zero  \n"
                 "Z: Set encoder 2 position to zero  \n"
                 "p: print encoder 1 position  \n"
                 "P: print encoder 2 position  \n"
                 "d: print encoder 1 delta to puTTY  \n"
                 "D: print encoder 2 delta to puTTY  \n"
                 "m: prompt user to enter duty cycle for motor 1  \n"
                 "M: prompt user to enter duty cycle for motor 2  \n"
                 "g: collect encoder 1 data for 30 seconds, then print   \n"
                 "G: collect encoder 2 data for 30 seconds, then print   \n"
                 "s or S: end data collection early  \n"
                 "c or C: clear fault raised by motor driver  \n")
             
            self.transition_to(S1_wait)
            
        if self.state == S1_wait:   
           #wait state, waits for user input  
            if (utime.ticks_diff(utime.ticks_us(), self.next_check)) >= 0:
                
                if self.ser.any():
                    char_in = self.ser.read(1).decode()
                      
                    if char_in == "z":
                        self.z_flag_1.write(True)
                        print("the position of encoder 1 will be zeroed")                       #in task_encoder print("task has been zeroed")
                    elif char_in == "Z":
                        self.z_flag_2.write(True)
                        print("the position of encoder 2 will be zeroed")                       #in task_encoder print("task has been zeroed")

                    elif char_in == "p":                                                        #print current position of encoder 1
                        print(self.data_1.read()[pos])
                        
                    elif char_in == "P":                                                        #print current position of encoder 2
                        print(self.data_2.read()[pos])
                     
    
                    elif char_in == "d":                                                        #print delta of encoder 1
                        print(self.data_1.read()[delta])
                        
                    elif char_in == "D":                                                        #print delta of encoder 2
                        print(self.data_2.read()[delta])
                       

                    elif char_in == "m":
                      
                        self.dutyID = 1
                        self.transition_to(S4_duty)
                        self.my_str = ''
                        
                        
                    elif char_in == "M":
                      
                        self.dutyID = 2
                        self.transition_to(S4_duty)
                        self.my_str = ''
                      
                    
                    elif char_in == "c" or char_in == "C":
                        print("Fault cleared by user")
                        self.faultFlagShare.write(False)                                             #there are 2 different faultFlag objects. One is shared between tasks, one is from dirver to task_motor
                        
                    elif char_in == "g":
                        self.collectID = 1  
                        print('collecting data')     #denotes collecting encoder 1 data
                        self.start_time = utime.ticks_us()
                        self.transition_to(S2_collect)
                        
                    elif char_in == "G":
                        self.collectID = 2  
                        print('collecting data')                                                     #denotes collecting encoder 2 data
                        self.start_time = utime.ticks_us()
                        self.transition_to(S2_collect)
                        

                    elif self.faultFlagShare.read():
                        print("Task_User sees a fault!")
                        
                    elif char_in == "1":
                        self.collectID = 1
                        print('Input target speed')
                        self.my_str = ''
                        self.start_time = utime.ticks_us()
                        self.transition_to(S5_collectSR)
                        
                    elif char_in == "2":
                        self.collectID = 2
                        print('Input target speed')
                        self.my_str = ''
                        
                        self.start_time = utime.ticks_us()
                        self.transition_to(S5_collectSR)
                        
                        
                    elif char_in == "k":
                        self.Kp.write(1)        #in the future make this something that we can change
                        
                    
                    self.next_check = utime.ticks_add(self.next_check, self.period_check)       #update the next time the program will check user input
                    
            

        if self.state == S2_collect:                                                            #Collection state, periodically record the time, target encoders position and delta
                                      
            if (utime.ticks_diff(utime.ticks_us(), self.start_time) >= 5_000_000):              #determine if data collection has occured for set time, reports success, trasitions to print state
               print("Data collection complete")
               print("There are {:} data points".format(len(self.dataBin)))
               self.transition_to(S3_print)

            if self.ser.any():                                                                  #determine if user has manually ended data collection, reports success, transitions to print state
                char_in = self.ser.read(1).decode()
                if (char_in == 's' or char_in == 'S'):
                    self.transition_to(S3_print)
                    print("collection ended early")
                    print("There are {:} data points".format(len(self.dataBin)))
                else:
                    print("To interrupt data collection press /'s/' or /'S/' or wait 30 seconds")     
                    
                    

            if (utime.ticks_diff(utime.ticks_us(), self.next_data) >= 0):                       #determines if it is time to collect data

                if self.collectID == 1:
                    self.dataBin.append(self.data_1.read())
#                                                                   #adds tuple to the list of tuples containing the current time, poisition, and delta of encoder 1

                elif self.collectID == 2:
                    self.dataBin.append(self.data_2.read())
                else:                                                                           #only occurs if collect ID is not 1 or 2
                    raise ValueError("Invalid collect ID")
  
                
                self.next_data = utime.ticks_add(self.next_data, self.period_data)              #update the next time the program will collect data
  
                    

        if self.state == S3_print:                                                              #state during which the program prints the collected data
            if len(self.dataBin) == 0:                                                             #check if the data list is empty, report succes, transition to wait state
                print("all data has been printed!")
#                self.dataFile.write(self.plotTime)       <------ for plots
#                self.dataFile.write(self.plotVel)
                self.transition_to(S1_wait)
                
            else:    
                new_data = self.dataBin.pop(0)   #remove first item from data list and print
                print(new_data)
                #self.dataFile.write(new_data)
                #Plotting of closed loop tuning
                #x-axis values
#                self.plotTime.append(new_data[1])   <----- for plots
#                #y values
#                self.plotVel.append(self.o_meas_1)
#                
                

                
#                #plotting
#                plt.plot(self.plotTime, self.plotVel)
#                #formatting plot
#                plt.xlabel('Time')
#                plt.ylabel('Velocity')
#                plt.title('Tuning Improvement')
        
        if self.state == S4_duty:
            if (utime.ticks_diff(utime.ticks_us(), self.next_check)) >= 0:

                
                if self.ser.any():
                    char_in = self.ser.read(1).decode()
  
                    if char_in.isdigit() == True:                   #The string is a digit
                        self.my_str += char_in 
                        
                                  #the string is not a digit
                    elif char_in == '-' and len(self.my_str) == 0:                              #the character is a -  
                        self.my_str += char_in
                            
                    elif  char_in == '\b' and len(self.my_str) != 0:
                        self.my_str = self.my_str.rstrip(self.my_str[-1])
                
                    elif char_in == '\r' or char_in == '\n':
                        #check if user input is valid
                        self.userInput = float(self.my_str)
                        
                        print(self.userInput)
                        
                        if self.userInput > 100:
                            print("duty lowered to 100")
                            self.userInput = 100
                        elif self.userInput < -100:
                            print("duty raised to -100")
                            self.userInput = -100

                        
                        if self.dutyID == 1:                           
                            self.DC_val_1.write(self.userInput)
                            self.transition_to(S1_wait)
                            
               
                        elif self.dutyID == 2:                          
                            self.DC_val_2.write(self.userInput)
                            self.transition_to(S1_wait)
                        
                        else:
                            raise ValueError('Invalid Duty ID')

                self.next_check = utime.ticks_add(self.next_check, self.period_check)       #update the next time the program will check user input

        if self.state == S5_collectSR: #rename this?
            if (utime.ticks_diff(utime.ticks_us(), self.next_check)) >= 0:
                
                if self.ser.any():
                    
                    char_in = self.ser.read(1).decode()
  
                    if char_in.isdigit() == True:                   #The string is a digit
                        self.my_str += char_in 
                        
                                  #the string is not a digit
                    elif char_in == '-' and len(self.my_str) == 0:                              #the character is a -  
                        self.my_str += char_in
                            
                    elif  char_in == '\b' and len(self.my_str) != 0:
                        self.my_str = self.my_str.rstrip(self.my_str[-1])
                
                    elif char_in == '\r' or char_in == '\n':
                        
                        self.userInput = float(self.my_str)
                        print(self.userInput)
                     
                        if self.collectID == 1:
                            self.o_ref_1.write(self.userInput)
                            self.transition_to(S1_wait)
                        
                        if self.collectID == 2:  
                            self.o_ref_2.write(self.userInput)
                            self.transition_to(S1_wait)
                
                self.next_check = utime.ticks_add(self.next_check, self.period_check)       #update the next time the program will check user input
        

                
        if self.state == S6_printSR:
            if (utime.ticks_diff(utime.ticks_us(), self.next_check)) >= 0:
                pass     
            
    def transition_to(self, new_state):
        """ @brief              Switch from current state to target
            @details           Switch from current state to target state.
            @param  new_state  The number or constant associated with the target state to transition to
        """
        self.state = new_state
        
        

