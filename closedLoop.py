""" @file                       closedLoop.py
    @brief                      A closed loop controller.
    @details                    Contains the class ClosedLoop
    @author                     Tim Matista
    @author                     Kendall Chappell
    @date                       November 15, 2021
"""

class ClosedLoop:
    ''' @brief      A class for controlling the velocity of a motor with a closed loop.
        @details    Takes data from shares for the measured and desired 
                    velocity. Outputs data to a share for the motor duty cycle.
                    Stores a proportional gain constant. The available methods
                    will update the motor's duty cycle, set and return the 
                    current proportional gain constant, and enable and
                    disable the controller.
    '''
    def __init__(self, motorID, Kp, o_ref, o_mea, DC_val):
        ''' @brief      Constructs a closed loop controller.
            @details    The closed loop controller object takes data from shares
                        for the measured and desired velocity. It outputs data
                        to a share for the motor duty cycle. It stores a
                        proportional gain constant.
            @param      Kp                      Initial proportional gain constant (%%duty/(rad/s))
            @param      o_mea                   Share to store desired motor velocity (rad/s).
            @param      o_ref                   Share to store measured motor velocity (rad/s).
            @param      DC_val              The share holding the duty cycle values to be set for motor.
        '''
        self.motorID = motorID
        self.Kp = Kp
        self.o_mea = o_mea
        self.o_ref = o_ref
        self.DC_val = DC_val
        self.enabled = True
    
    def update(self):
        ''' @brief      Updates the control loop.
            @details    Uses the proportional gain constant to set the motor's duty cycle.
                        The error between the desired velocity and the meausured
                        velocity is computed, then multiplied by the proportional gain.
                        A saturation limit is then applied to this since the motor
                        cannot run above 100% duty cycle.
        '''
        error = self.o_ref.read() - self.o_mea.read()
        if self.enabled:
            
            if error * (self.Kp.read()) >= 0:
                
                duty = min(error * self.Kp.read(), 100)
            else:
                duty = max(error * self.Kp.read(), -100)
            self.DC_val.write(duty)
            
            
    def get_Kp(self):
        ''' @brief      Returns the proportional gain constant.
            @details    Returns the current proportional gain for the closed
                        loop controller. It has units of %%duty/(rad/s).
            @return     Proportional gain constant Kp
        '''
        return self.Kp
    
    def set_Kp(self, Kp):
        ''' @brief      Sets the proportional gain constant.
            @details    Sets the current proportional gain for the closed
                        loop controller. It has units of %%duty/(rad/s).
            @param      Kp  Proportional gain constant (%%duty/(rad/s))
        '''
        self.Kp.write(Kp)
        
    def enable(self):
        ''' @brief      Enables the closed loop controller.
            @details    Makes it so that the closed loop controller sets the
                        duty cycle for the motor. Don't try to set the duty
                        cycle manually when the controller is enabled.
        '''
        self.enabled = True
    
    def disable(self):
        ''' @brief      Disables the closed loop controller.
            @details    Makes it so that the closed loop controller will not
                        write to DC_val. This allows motor duty cycle to
                        be set by something else, like our manual input.
        '''
        self.enabled = False
        self.DC_val.write(0)
    
