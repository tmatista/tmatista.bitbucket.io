var files_dup =
[
    [ "BNO055.py", "BNO055_8py.html", [
      [ "BNO055.BNO055", "classBNO055_1_1BNO055.html", "classBNO055_1_1BNO055" ]
    ] ],
    [ "closedLoop.py", "closedLoop_8py.html", [
      [ "closedLoop.ClosedLoop", "classclosedLoop_1_1ClosedLoop.html", "classclosedLoop_1_1ClosedLoop" ]
    ] ],
    [ "encoder.py", "encoder_8py.html", [
      [ "encoder.Encoder", "classencoder_1_1Encoder.html", "classencoder_1_1Encoder" ]
    ] ],
    [ "lightPattern.py", "lightPattern_8py.html", "lightPattern_8py" ],
    [ "main.py", "main_8py.html", null ],
    [ "mainpage.py", "mainpage_8py.html", null ],
    [ "motor_driver.py", "motor__driver_8py.html", "motor__driver_8py" ],
    [ "panel_driver.py", "panel__driver_8py.html", "panel__driver_8py" ],
    [ "shares.py", "shares_8py.html", [
      [ "shares.Share", "classshares_1_1Share.html", "classshares_1_1Share" ],
      [ "shares.Queue", "classshares_1_1Queue.html", "classshares_1_1Queue" ]
    ] ],
    [ "task_controller.py", "task__controller_8py.html", [
      [ "task_controller.Task_Controller", "classtask__controller_1_1Task__Controller.html", "classtask__controller_1_1Task__Controller" ]
    ] ],
    [ "task_encoder.py", "task__encoder_8py.html", "task__encoder_8py" ],
    [ "Task_Motor.py", "Task__Motor_8py.html", [
      [ "Task_Motor.Task_Motor", "classTask__Motor_1_1Task__Motor.html", "classTask__Motor_1_1Task__Motor" ]
    ] ],
    [ "Task_User.py", "Task__User_8py.html", "Task__User_8py" ]
];