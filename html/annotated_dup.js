var annotated_dup =
[
    [ "BNO055", null, [
      [ "BNO055", "classBNO055_1_1BNO055.html", "classBNO055_1_1BNO055" ]
    ] ],
    [ "closedLoop", null, [
      [ "ClosedLoop", "classclosedLoop_1_1ClosedLoop.html", "classclosedLoop_1_1ClosedLoop" ]
    ] ],
    [ "encoder", null, [
      [ "Encoder", "classencoder_1_1Encoder.html", "classencoder_1_1Encoder" ]
    ] ],
    [ "motor_driver", null, [
      [ "DRV8847", "classmotor__driver_1_1DRV8847.html", "classmotor__driver_1_1DRV8847" ],
      [ "Motor", "classmotor__driver_1_1Motor.html", "classmotor__driver_1_1Motor" ]
    ] ],
    [ "panel_driver", null, [
      [ "Panel", "classpanel__driver_1_1Panel.html", "classpanel__driver_1_1Panel" ]
    ] ],
    [ "shares", null, [
      [ "Queue", "classshares_1_1Queue.html", "classshares_1_1Queue" ],
      [ "Share", "classshares_1_1Share.html", "classshares_1_1Share" ]
    ] ],
    [ "task_controller", null, [
      [ "Task_Controller", "classtask__controller_1_1Task__Controller.html", "classtask__controller_1_1Task__Controller" ]
    ] ],
    [ "task_encoder", null, [
      [ "Task_Encoder", "classtask__encoder_1_1Task__Encoder.html", "classtask__encoder_1_1Task__Encoder" ]
    ] ],
    [ "Task_Motor", null, [
      [ "Task_Motor", "classTask__Motor_1_1Task__Motor.html", "classTask__Motor_1_1Task__Motor" ]
    ] ],
    [ "Task_User", null, [
      [ "Task_User", "classTask__User_1_1Task__User.html", "classTask__User_1_1Task__User" ]
    ] ]
];