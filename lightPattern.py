
'''@file                    lightPattern.py
   @brief                   Square, sine, and sawtooth wave LED pattern. 
   @details                 After initialization, this program opens a state which will wait for the first button push. 
                            Once the button is pushed, the program will enter the square wave state. The square wave
                            state will run until the button is pushed again, in which case it will switch to the sine
                            wave state. The program will remain in the sine wave state until the button is pressed, then
                            the program will enter the sawtooth wave state. Again, the program will remain in the sawtooth 
                            wave state until the button is pressed. When the button is pressed, the program will return 
                            the square wave state again. This pattern will continue until the program is terminated. 
                            \image html lightPatternFSM.jpg
   @author                  Kendall Chappell
   @author                  Timothy Matista
   @date                    October 8, 2021
'''

#Import
import time
import math
import pyb

#set up nucleo pins, frequency, and internal clock
pinC13 = pyb.Pin (pyb.Pin.cpu.C13)          #blue button
pinA5 = pyb.Pin (pyb.Pin.cpu.A5)            #green LED (LD2)
tim2 = pyb.Timer(2, freq = 20000)
t2ch1 = tim2.channel(1, pyb.Timer.PWM, pin=pinA5)
t2ch1.pulse_width_percent(0)                #inital PWM of 0

# Functions for this program
def onButtonPressFCN(IRQ_src):              #flags button as pushed 
    '''@brief           Function for button push.
       @details         This function flags the button as pushed and annouces that it has been pushed. 
    '''
    global buttonPushed
    buttonPushed = True                     #needs to be reset within script
    print('The button has been pressed!')   #confims button press
    
    
def resetTimer():                           #resets startTime
    '''@brief           Reset the timer
       @details         This function will be used to reset the timer in states 2 through 4 (all states with an 
                        LED flashing output). 
    '''
    global startTime
    startTime=time.ticks_ms()               #used as reference time for function


def elapsedTime(currentTime):               #check time since startTime   
    '''@brief           Current state's duration
       @details         This function determines the elapsed time since entering the current state.
    '''   
    global duration
    duration = (time.ticks_diff(currentTime,startTime))/1000    #how long since startTime in seconds
           
    
if __name__ == '__main__':
    
    state = 0                               #initial state of the program
    runs = 0                                #number of iterations run
    buttonPushed = False                    #initial button state

    #interrupt for when the button is pressed. Calls onButtonPushFCN()
    ButtonInt = pyb.ExtInt(pinC13, mode=pyb.ExtInt.IRQ_FALLING,pull=pyb.Pin.PULL_NONE, callback=onButtonPressFCN)
    
    while True:
        try:
            
            if state == 0:      #run state 0 code
               
                # Intro message + instructions
                print("Hello and welcome to blinky lights and stuff!");
                print("\nPress the blue button to cycle through a square, sine, and sawtooth wave.")
                state=1      #Transition to state 1 (no button)
                
            elif state == 1:    #run state 1 code
                print("Press the Blue button to begin. Press ctrl c to stop the program.\n");
               
                while True:                     #loop until button is pushed
                    if buttonPushed == True:    #wait for button push
                        state = 2;              #Transition to state 2 (button push)
                        buttonPushed = False    #resets button state
                        break
                    
            elif state == 2:    #run state 2 code
                print("Displaying Square Wave\n")
                resetTimer()                            #restart timer for this state 
 
                while True:                             #loop until button is pushed                        
                    currentTime = time.ticks_ms()
                    elapsedTime(currentTime)            #updates duration each loop
                                       
                    bright = (duration%1.0 < 0.5)*100   #produces square wave pattern
                    
                    t2ch1.pulse_width_percent(bright)   #PWM based on "bright"
                    if buttonPushed == True:            #wait for button push
                            state = 3                   #Transition to state 3 (button push)
                            buttonPushed = False        #resets button state
                            break
                    
            elif state == 3:    #run state 3 code
                print("Displaying Sine Wave\n")
                resetTimer()                            #restart timer for this state 
                
                while True:                             #loop until button is pushed
                    currentTime = time.ticks_ms()
                    elapsedTime(currentTime)            #updates duration each loop          
                    
                    bright = (0.5+0.5*math.sin(duration*math.pi/5))*100 #produces sine wave pattern
                    
                    t2ch1.pulse_width_percent(bright)   #PWM based on "bright"
                    if buttonPushed == True:            #wait for button push
                            state = 4                   #Transition to state 4 (button push)
                            buttonPushed = False        #resets button state
                            break             
                
            elif state == 4:    #run state 4 code 
                print("Displaying Sawtooth Wave\n")
                resetTimer()                            #restart timer for this state 
                
                while True:                             #loop until button is pushed
                    currentTime = time.ticks_ms()
                    elapsedTime(currentTime)            #updates duration each loop   
                    
                    bright = (duration%1.0)*100         #produces sawtooth wave pattern
                    
                    t2ch1.pulse_width_percent(bright)   #PWM based on "bright"
                    if buttonPushed == True:
                            state = 2;                  #return to state 2 (button push)
                            buttonPushed = False        #resets button state
                            break             
                
            runs += 1;    #Tracks how many times the pattern has run (not used, only tracked)
            
        except KeyboardInterrupt:       # Announces if the program has been terminated
            print("Program Terminated\n")
            break
        
    
