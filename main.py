""" @file           main.py 
    @brief          Main file for running an encoder display program
    @details        Runs an encoder task and user task which allow the user to collect positional data
                    from one or more encoders. The user is able to zero the position, display position and delta,
                    and collect data for up to 30 seconds.
                    PYTHON FILES LOCATION: https://bitbucket.org/kcchappe/me305lab/src/master/Lab%202/
                    
                    References: Tori Bornino, Jackson McLaughlin
    @author         Tim Matista
    @author         Kendall Chapell
    @date           10/12/2021
    @page           

"""
#import statements

import shares
import task_encoder
import task_user
import motor_driver
import task_motor
import task_controller
import closedloop

encoderPeriod = 1_000
userPeriod = 10_000
collectPeriod = 100_000
motorPeriod = 1_000
controllerPeriod = 1_000


if __name__ == '__main__':
    
    #initialize shares for encoder 1
    position_1 = shares.Share(0)
    delta_1 = shares.Share(0)
    z_flag_1 = shares.Share(False)
    DC_val_1 = shares.Share(0)
    o_mea_1 = shares.Share(0)
    o_ref_1 = shares.Share(0)
    data_1 = shares.Share()
    Kp_1 = shares.Share(1)
    
    #initialize shares for encoder 2
    position_2 = shares.Share(0)
    delta_2 = shares.Share(0)
    z_flag_2 = shares.Share(False)
    DC_val_2 = shares.Share(0)
    o_mea_2 = shares.Share(0)
    o_ref_2 = shares.Share(0)
    data_2 = shares.Share()
    Kp_2 = shares.Share(1)
    
    faultFlagShare = shares.Share(False)

    srQueue = shares.Queue()
    
    dataQueue = shares.Queue()
    
    motor_drv = motor_driver.DRV8847(faultFlagShare) 
    motor_drv.enable()
    
    
    
    closedLoop1 = closedloop.ClosedLoop(1, Kp_1, o_ref_1, o_mea_1, DC_val_1)
    
    closedLoop2 = closedloop.ClosedLoop(2, Kp_2, o_ref_2, o_mea_2, DC_val_2)
    
    
    USER = task_user.Task_User(userPeriod, collectPeriod, 
                               z_flag_1, z_flag_2, 
                               data_1, data_2, 
                               faultFlagShare, 
                               DC_val_1, DC_val_2, 
                               o_mea_1, o_mea_2, 
                               o_ref_1, o_ref_2,
                               closedLoop1, closedLoop2,
                               dataQueue, srQueue)
                                
        
    CONT1 = task_controller.Task_Controller(controllerPeriod, closedLoop1)
    
    CONT2 = task_controller.Task_Controller(controllerPeriod, closedLoop2)
    
    
    ENC1 = task_encoder.Task_Encoder(encoderPeriod,  z_flag_1, data_1, o_mea_1, 1)
        #task_encoder_1 arguments: (period, encoder1, pos1, del1, zflag1, name1 = NONE)         
    ENC2 = task_encoder.Task_Encoder(encoderPeriod,  z_flag_2, data_2, o_mea_2, 2)
        #task_encoder_2 arguments: (period, encoder2, pos2, del2, zflag2, name2 = NONE)

    MOT1 = task_motor.Task_Motor(1, DC_val_1, faultFlagShare, motorPeriod, motor_drv)
        #task_motor_1 arguments: (period, motor1, DC_val, faultFlagShare)
    MOT2 = task_motor.Task_Motor(2, DC_val_2, faultFlagShare, motorPeriod, motor_drv)
        #task_motor_2 arguments: (period, motor2, DC_val, faultFlagShare)
        

 
    #run tasks
    while(True):
        try:
            CONT1.run()
            CONT2.run()
            ENC1.run()
            ENC2.run()
            MOT1.run()
            MOT2.run()
            USER.run()
            

        except KeyboardInterrupt:
            motor_drv.disable()
            print('Program Terminating')
            break
    #happens after keyboard interrupt occurs
  
