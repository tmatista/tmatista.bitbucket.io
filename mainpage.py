'''@file                mainpage.py
   @brief               Main page of documentation site.
   @details             Contains information on each lab and links to the respective code.

   @mainpage

   @section sec_intro   Introduction
                        Hello there! This website houses Tim Matista's code documentation for ME 305
                        The repository with all of the source code can be found at:
                        https://bitbucket.org/tmatista/me305_labs

   @section sec_fib     Lab 0: Fibonacci Sequence
                        This is only posted to see if this site is working.
                        See fibonacciTM.py

   @section sec_led     Lab 1: Light Pattern
                        This code makes the tiny LED on the Nucleo light up in different ways and is cycled by pressing the blue button on the board.
                        Includes square, sinusoidal, and sawtooth wave patterns.
                        See lightPattern.py for code and FSM


   @section sec_enc     Lab 2: Encoder Display
                        This lab we made an encoder driver
                        @ref encoder.Encoder "Encoder", encoder task
                        @ref task_encoder.Task_Encoder "Task_Encoder" for updating the encoded shaft's position.
                        Initially this code printed shaft position and change in position, but has been updated to pass that information around to various files. 
                        This week also saw the introduction of our user task
                        @ref Task_User.Task_User "Task_User" which handles all inputs from the user and is the FSM that drives the whole project.
                        \image html Lab2TaskDiagram.jpg width=800px

   @section sec_mot     Lab 3: Motor Test
                        This lab we created our motor driver @ref motor_driver.DRV8847 "DRV8847"
                        which was used to create motor objects @ref motor_driver.Motor "Motor".
                        The motor objects were given a duty cycle (PWM) which was fed through @ref Task_motor.Task_motor "Task_Motor".
                        The user and encoder tasks were updated to account for these new functions.
                        A new class @ref shares.Share "Share" is used to pass information between tasks.
                        \image html Lab3TaskDiagram.jpg width=800px

   @section sec_clc     Lab 4: Closed Loop Control
                        This lab introduced feedback control in the class
                        @ref closedLoop.ClosedLoop "ClosedLoop" which was implimented through the task
                        @ref task_controller.Task_Controller "Task_Controller". This allows for the code to verify
                        that the motor is running as close to the reqeusted speed as possible. This will be very important
                        for precision control.
                        The user task was updated to account for these new functions.
                        \image html Lab4TaskUserFSM.jpg width=800px

   @section sec_imu     Lab5: BNO055 IMU Driver
                        In addition to further understanding the motor behavior, it is important to know what the platfrom is doing in response.
                        This lab introduced the 9 axis IMU that is used in this project. The imu is driven by a driver
                        @ref BNO055.BNO055 "BNO055"
                        which handles calibration, euler angle calculation, and angular velocities.

   @section sec_rtp     Final Lab: RTP and Project Integration
                        The last file added to the project is the driver
                        @ref panel_driver.Panel "Panel"
                        that handles interpreting information from the resistive touch panel.
                        Putting all of the components of this project together allows a ball to be balanced on the panel and gently be brought to the center.
                        Unfortunately, this project is unfinished due to personal reasons. I hope to one day finish this project, but this is all I have for now.



   @section sec_HW      Homework 2 and 3
                        This homework was the modeling of the project and serves as the basis for the
                        system as a whole. Kendall did an amazing job on the hand calculations and much of the linearization.
                        \image html HW1.jpg width=800px
                        \image html HW2.jpg width=800px
                        \image html HW3.jpg width=800px
                        \image html HW4.jpg width=800px
                        \image html HW5.jpg width=800px
                        \image html HW6.jpg width=800px
                        \image html HW0x02.jpg width=800px
                        \image html HW0x03.jpg width=800px


   
   @author              Tim Matista
   @author              Kendall Chappell

   @copyright           Special thanks to Tori and Jackson for helping throughout the process. Much of this code is inspired by theirs.

   @date                October 8th, 2021
'''
