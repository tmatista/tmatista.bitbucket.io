""" @file           motor_driver.py
    @brief          Contains the DRV8847 and Motor classes.
    @details        Contains the DRV8847 class which creates a driver object. This driver is used in 
                    conjunction with the Motor class to create a Motor object. Feeds PWM signals to 
                    the motor, enable, disables, and facilitates fault detection of the motor.
    @author         Tim Matista
    @author         Kendall Chapell
    @date           10/26/2021
"""

import pyb
import utime

class DRV8847:
    """ @brief          Driver class to create driver object
        @details        This driver class is used with the Motor class to create motor objects. 
                        Handles fault detection, enabling and disableing motor in addition to 
                        feeding it PWM signals.
    """    

    def __init__(self, faultFlagShare):
        """ @brief   A motor driver class for DRV8874 Texas Instruments Motor
            @details Objects made of this class can configure the DRV8847 Driver and can create motor objects
                     using the Motor class
            @param   faultFlagshare     Share object that is raised when fault is detected.
        """

        tim = pyb.Timer(3, freq = 20_000)
        
        pinB4 = pyb.Pin(pyb.Pin.cpu.B4)
        pinB5 = pyb.Pin(pyb.Pin.cpu.B5)
        pinB0 = pyb.Pin(pyb.Pin.cpu.B0)
        pinB1 = pyb.Pin(pyb.Pin.cpu.B1)

        self.pinFault = pyb.Pin(pyb.Pin.cpu.B2)
        self.faultINT = pyb.ExtInt(self.pinFault, mode= pyb.ExtInt.IRQ_FALLING, pull = pyb.Pin.PULL_NONE, callback=self.fault_cb) 
        
        self.pinSleep = pyb.Pin(pyb.Pin.cpu.A15, pyb.Pin.OUT_PP)

        self.faultFlagShare = faultFlagShare
        
        self.t3ch1 = tim.channel(1, pyb.Timer.PWM, pin=pinB4)
        self.t3ch2 = tim.channel(2, pyb.Timer.PWM, pin=pinB5)
        self.t3ch3 = tim.channel(3, pyb.Timer.PWM, pin=pinB0)
        self.t3ch4 = tim.channel(4, pyb.Timer.PWM, pin=pinB1)

        
    def enable (self):
        """ @brief          Enable driver by raising sleep pin
            @details        Enable driver by raising sleep pin. Temporarily disables fault interrupt 
                            to counter a common hardware issue that displays fault on startup. 
        """

        if not self.faultFlagShare.read():          #will not enable if there is a fault
            self.faultINT.disable()                 # Disable fault interrupt
            self.pinSleep.high()                    # Re-enable the motor driver
            utime.sleep_us(25)                      # Wait for the fault pin to return high
            self.faultINT.enable()                  # Re-enable the fault interrupt
        
        else:
            print("Clear fault to enable")


    def disable (self):
        """ @brief         Disable driver by lowering sleep pin.
        """
        self.pinSleep.low()         


    def fault_cb (self, IRQ_src):
        """ @brief          Determines reaction when fault is detected
            @details        Disables motor and sets faultFlag to true, denoting that a fault 
                            has been detected. Sets duty cycle to 0 and calls disable().
        """        

        print("Fault Dectected")
        self.disable()
     
        self.t3ch1.pulse_width_percent(0)
        self.t3ch2.pulse_width_percent(0)
        self.t3ch3.pulse_width_percent(0)
        self.t3ch4.pulse_width_percent(0)
        self.disable()
        self.faultFlagShare.write(True)

        
    def motor (self, motorID):
        """ @brief                Pass requiset inforation to Motor class to create a Motor object.
            @return   the_motor   the newly created motor object is returned
        """
        
        if motorID == 1:
            return Motor(self.t3ch1, self.t3ch2)
        
        elif motorID == 2:
            return Motor(self.t3ch3, self.t3ch4)
        
        else:
            raise ValueError ('Invalid motor ID. Please choose either 1 or 2')

class Motor:
    """ @brief          Create Motor object, interpret duty cycle
        @details        Handles the creation and duty cycle update of motor objects. Determines if 
                        valid duty cycle is requested. In invalid cycle requested, "Clamps" value to within -100 to 100 and 
                        interpret for motor control.
    """

    
    def __init__ (self, ch1, ch2):
         """ @brief             Create Motor object
             @details           Create Motor object, comprised of two channels, each of which can 
                                be powered by PWM.  
             @param    ch1      The timer object for channel 1
             @param    ch2      the timer object for channel 2
         """ 
         self.ch1 = ch1
         self.ch2 = ch2
         
     
    def set_duty (self, duty):
         """ @brief          Sets Duty cycle for Motor object (PWM).
             @details        Sets Duty cycle for Motor object (PWM). Adjusts value to be accepted by PWM function.
                             "clamps" values to +100 and -100 and inteprets this for motor control.
         """
         self.duty = duty

         if self.duty > 0:
            if self.duty <= 100: 
                self.ch1.pulse_width_percent(self.duty)
                self.ch2.pulse_width_percent(0)
            elif self.duty > 100:
                self.duty = 100
                self.ch1.pulse_width_percent(100)
                self.ch2.pulse_width_percent(0)
            else:
                print("Misinput High")
                 
         elif self.duty < 0:
            if self.duty >= -100:
                self.ch1.pulse_width_percent(0)
                self.ch2.pulse_width_percent(-1*self.duty)
            elif self.duty < -100:
                self.duty = -100
                self.ch1.pulse_width_percent(0)
                self.ch2.pulse_width_percent(100)
            else:
                print("Misinput Low")
       
         elif self.duty == 0:
            self.ch1.pulse_width_percent(0)
            self.ch2.pulse_width_percent(0)
             
         else:
             raise ValueError('Invalid entry.')
             
     
if __name__ == '__main__':


    motorDriver = DRV8847()
    motor_1 = motorDriver(1)
    motor_2 = motorDriver(2)
    
    #enable the motor driver
    motorDriver.enable()
    
    motor_1.set_duty(60)
    motor_2.set_duty(60)
    
    



    
