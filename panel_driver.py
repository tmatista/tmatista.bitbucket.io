""" @file                       panel_driver.py
    @brief                      Driver to control 4 wire RTP
    @details                    Scans resistance in "XYZ" to confirm contact and contact position. 
    @author                     Tim Matista
    @author                     Kendall Chappell
    @date                       November 18, 2021
"""

import pyb
import utime

class Panel:
    ''' @brief                  A driver for controlling Resistive Touch Pannel (RTP)
        @details                Configures pins to check x position, y position, and touch confirmation.
        
    '''
    
    def __init__(self, pin1, pin2, pin3, pin4, width, length, center):   
        ''' @brief                  ...
            @details                ...
            @param  pin1    pin 1 of 4 on the RTP
            @param  pin2    pin 2 of 4 on the RTP
            @param  pin3    pin 3 of 4 on the RTP
            @param  pin4    pin 4 of 4 on the RTP
            @param  width   width of the RTP in mm
            @param  legth   length of the RTP in mm
            @param  center   center cordinates of RTP from bottom left corner. Used to define new reference point.
            
        '''
#        pin numbering folows x6 pinout see instructions
        self.YM = pyb.Pin(pin1)
        self.XM = pyb.Pin(pin2)
        self.YP = pyb.Pin(pin3)
        self.XP = pyb.Pin(pin4)

        self.OUT = pyb.Pin.OUT_PP
        self.IN = pyb.Pin.IN
        self.FLOAT = self.IN
#        self.t_flag = False

        adcLimit = 4095
        self.xScale = width/adcLimit
        self.yScale = length/adcLimit
        self.center = center
        
    def scanX(self):
        """ @brief                  Configures pins to scan RTP for x position of contact.
        """
            
        #float yp
        #high xp
        #low xm
        #adc ym
        
        YP = pyb.Pin(self.YP, self.FLOAT)
        XP = pyb.Pin(self.XP, self.OUT, value = 1)
        XM = pyb.Pin(self.XM, self.OUT, value = 0)
        YM = pyb.ADC(self.YM)
        
        utime.sleep_us(5)
        xData = int(YM.read() * self.xScale - self.center[0])
        return xData
        
    
    def scanY(self):
        """ @brief                  Configures pins to scan RTP for y position of contact.
        """
        
        #high yp
        #float xp
        #adc xm
        #low ym
        
        YP = pyb.Pin(self.YP, self.OUT, value = 1)
        XP = pyb.Pin(self.XP, self.FLOAT)
        XM = pyb.ADC(self.XM)
        YM = pyb.Pin(self.YM, self.OUT, value = 0)
        
        utime.sleep_us(5)
        
        yData = int(XM.read() * self.yScale - self.center[1])
        return yData
    
    def scanZ(self):  
        """ @brief                  Configures pins to scan RTP for contact.
        """
        
        #high yp
        #float xp
        #low xm
        #adc ym

        YP = pyb.Pin(self.YP, self.OUT, value = 1)
        XP = pyb.Pin(self.XP, self.FLOAT)
        XM = pyb.Pin(self.XM, self.OUT, value = 0)
        YM = pyb.ADC(self.YM)

        utime.sleep_us(5)
        zData = YM.read()
#        print(zData)
        if zData < 3500:
            zData = 1
#            self.t_flag = True
        elif zData >= 3500:
            zData = 0
#            self.t_flag = False   
        return zData
    
        
    def run(self):
        """ @brief                  Call scanX(), scanY(), and scanZ()
            @return                 Returns tuple "touchData" containing postition of contact and contact confirmation.
        """
#        if self.t_flag == True:
#            touchData = (self.scanX(), self.scanY(), self.scanZ())
#            print(touchData)
#        else:
#            print("no touchy")
#            pass

        touchData = (self.scanX(), self.scanY(), self.scanZ())
#        self.scanX()
        return touchData

if __name__ == "__main__":  
    
#    for testing the driver
    pin1 = pyb.Pin.cpu.A0
    pin2 = pyb.Pin.cpu.A1
    pin3 = pyb.Pin.cpu.A6
    pin4 = pyb.Pin.cpu.A7 
    TouchPanel = Panel(pin1, pin2, pin3, pin4, 176, 100, (88,50))
    
    
#    for checking the time it takes to run the driver
    startTime = utime.ticks_us()
    for lp in range(100):
        Time = utime.ticks_us()
        TouchPanel.run()
    endTime = utime.ticks_us()
    runTime = utime.ticks_diff(endTime, startTime)/100
    print(runTime)
    
