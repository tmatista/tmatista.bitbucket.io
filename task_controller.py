''' @file                       task_controller.py
    @brief                      A task for running a control loop
    @details                    Contains the class Task_Controller.
    @author                     Tim Matista
    @author                     Kendall Chappell
    @date                       November 15, 2021
    \image html Lab4Graph.jpg
    This plot shows the output speed consistantly adjusting to stay close to the target speed.
'''
import utime

class Task_Controller:
    ''' @brief      A task for setting up closed loop control of the motor.
        @details    Compares current motor speed to targeted motor speed to closed loop control.

                    
    '''

    def __init__(self, period, closedLoop):
        ''' @brief      Initializes the closed loop task task.
            @param      period      The period, in microseconds, to run this task.
            @param      closedLoop  The closed loop object.
        '''
        self.period = period
        self.nextTime = utime.ticks_add(utime.ticks_us(), self.period)
        self.closedLoop = closedLoop
        
        
    def run(self):
        '''
            @details    Updates the closed loop.
        '''
        if utime.ticks_diff(utime.ticks_us(), self.nextTime) >= 0:
            self.nextTime = utime.ticks_add(self.nextTime, self.period)
            self.closedLoop.update()
