""" @file                       task_encoder.py
    @brief                      A task for updating an encoder.
    @details                    Includes the class Task_Encoder.
    @author                     Tim Matista
    @author                     Kendall Chappell
    @date                       November 14, 2021
"""

import math
import encoder
import pyb
import utime

# Counts per revolution of encoder
CPR = 4000.0

class Task_Encoder:
    """ @brief                  A task for updating an encoder.
        @details                This class is a task for updating quadrature encoders.
                                Use in conjunction with encoder.py
   """

    def __init__(self, period, z_flag, data, o_mea, encoderID):
        ''' @brief      Initialize an encoder task.
            @details    This will instantiate an Encoder object which stores its
                        position and delta data in a Share. It sets up the task
                        to run at period.
            @param      period              Period, in microseconds, at which to take data from the encoder.
            @param      z_flag              Share indicating the user wants to zero the encoder.
            @param      data                Share of encoder data as a tuple (time, position, delta).
            @param      o_mea               Share to store measured velocity (rad/s)
            @param      encoderID           Which encoder to use (1 or 2). This selects 
                                            the pins and timers to use: timer 4 on pins
                                            B6 and B7 for encoder 1, and timer 8 on pins
                                            C6 and C7 for encoder 2. These are specific to the hardward used.
            
        '''


        if encoderID == 1:
            self.currentEncoder = encoder.Encoder(4, pyb.Pin.cpu.B6, pyb.Pin.cpu.B7)
        
            
        elif encoderID == 2:
            self.currentEncoder = encoder.Encoder(8, pyb.Pin.cpu.C6, pyb.Pin.cpu.C7)
            
        else:
            raise ValueError("Invalid Motor ID. Motor ID should be 1 or 2.")

        self.period = period
        self.z_flag = z_flag
        self.data = data
        self.o_mea = o_mea

        self.nextTime = utime.ticks_add(utime.ticks_us(), period)



    def run(self):
        """ @brief       Run the encoder task.
            @details     Running this task will update the shared object data, 
                         which contains (time, position, delta). Additionally checks 
                         for a raised z_flag, denoting the user wishes to zero the encoder.
        """

        if utime.ticks_diff(utime.ticks_us(), self.nextTime) >= 0:                  #check if time to run
            self.nextTime = utime.ticks_add(self.nextTime, self.period)             #update nextTime

            if (self.z_flag.read()):                                               
                self.currentEncoder.set_position(0)
                self.z_flag.write(False)
            
            #add filter to data.write() so that the info going in is ready for unpacking in other tasks
            self.currentEncoder.update()
#            self.data.write((utime.ticks_us(), self.currentEncoder.get_position(), self.currentEncoder.get_delta()))          
            self.o_mea.write(self.currentEncoder.get_delta() / (self.period/1000000) * 2.0 * math.pi / CPR)           
            self.data.write((utime.ticks_us(), self.currentEncoder.get_position(), self.o_mea.read()))
